// import React from "react";
import { CssBaseline } from "@mui/material";
import { AuthPage } from "pages/auth";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { store } from "./app/store/store";

// import Header from "./Header";

const App = () => (
  <Provider store={store}>
    <CssBaseline />
    <BrowserRouter>
      <div>
        <AuthPage />
      </div>
    </BrowserRouter>
  </Provider>
);
ReactDOM.render(<App />, document.getElementById("app"));
