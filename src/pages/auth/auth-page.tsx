// import { AuthBox } from "features/auth/ui";

import { AuthWidget } from "../../widgets/auth";
import type { TAuthWidgetProps } from "../../widgets/auth/types";
import { StyledAuthPageContainer } from "./styles";

export const AuthPage = (props: TAuthWidgetProps) => (
  <StyledAuthPageContainer>
    <AuthWidget {...props} />
  </StyledAuthPageContainer>
);
