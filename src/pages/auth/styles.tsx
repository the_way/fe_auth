import { styled } from "@mui/material";

export const StyledAuthPageContainer = styled("div")(() => ({
  display: "flex",
  width: "100%",
  height: "90vh",
  alignItems: "center",
  justifyContent: "center",
}));
