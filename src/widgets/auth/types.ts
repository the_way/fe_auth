import type { AnyAction } from "redux";
import type { GlobalStore } from '../../app/store/root-reducer'
type TAuthData = {
  username: string;
  password: string;
};

export type TForm = {
  mode: "signIn" | "signUp";
  signUp: TAuthData;
  signIn: TAuthData;
};

export type TAuthWidgetProps = {
  setAuthorized?: (flag: boolean) => AnyAction;
  ROUTES?: { HOME: string };
  selectAuthorized?: (state: GlobalStore) => boolean;
};
