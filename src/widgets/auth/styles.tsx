import { styled } from "@mui/material";

export const StyledTogglesBox = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "end",
  marginBottom: theme.spacing(5),
}));
