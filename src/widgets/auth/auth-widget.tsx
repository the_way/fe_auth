import { Typography } from "@mui/material";

import { AuthBox } from "../../features/auth/ui";
import {
  Button,
  ControlledInput,
  ControlledToggleGroup,
} from "shared_ui/remote";
import type { TAuthWidgetProps } from "./types";
import { StyledTogglesBox } from "./styles";
import { useLogic } from "./use-logic";

export const AuthWidget = (props: TAuthWidgetProps) => {
  const {
    handleRegistrationClick,
    handleLoginClick,
    isLogError,
    isRegError,
    toggleWatcher,
    control,
  } = useLogic(props);

  return (
    <AuthBox>
      <StyledTogglesBox>
        <ControlledToggleGroup
          name="mode"
          control={control}
          elementProps={{
            data: [
              { value: "signIn", label: "Login" },
              { value: "signUp", label: "Registration" },
            ],
            shouldEnforceValueSet: true,
            exclusive: true,
          }}
        />
      </StyledTogglesBox>
      {toggleWatcher === "signUp" ? (
        <>
          <Typography align="center" variant="h5">
            Registration Form
          </Typography>
          <ControlledInput
            // need, because of bug with "value duplicate" is appearing
            key="signUp.username"
            control={control}
            name="signUp.username"
            elementProps={{
              label: "username",
              error: isRegError,
              name: "regName",
            }}
          />
          <ControlledInput
            key="signUp.password"
            control={control}
            name="signUp.password"
            elementProps={{
              label: "password",
              error: isRegError,
              name: "regPass",
            }}
          />
          <Button onClick={handleRegistrationClick}>SIGN UP</Button>
        </>
      ) : (
        <>
          <Typography align="center" variant="h5">
            Login Form
          </Typography>{" "}
          <ControlledInput
            key="signIn.username"
            control={control}
            name="signIn.username"
            elementProps={{
              label: "username",
              error: isLogError,
              name: "logName",
            }}
          />
          <ControlledInput
            key="signIn.password"
            control={control}
            name="signIn.password"
            elementProps={{
              label: "password",
              error: isLogError,
              type: "password",
              name: "logPass",
            }}
          />
          <Button onClick={handleLoginClick}>SIGN IN</Button>
        </>
      )}
    </AuthBox>
  );
};
