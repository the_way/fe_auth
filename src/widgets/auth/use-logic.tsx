import { authApi } from "../../shared/api/auth-api";
import { useSignIn } from "../../features/auth/use-sign-in";

import { useForm } from "react-hook-form";
import type { TAuthWidgetProps, TForm } from "./types";

export const useLogic = (props: TAuthWidgetProps) => {
  const { signIn, isError: isLogError } = useSignIn(props);
  const [signUp, { isError: isRegError }] = authApi.useSignUpMutation();

  const { control, watch } = useForm<TForm>({
    defaultValues: {
      signIn: {
        username: "employer",
        password: "employer",
      },
    },
  });
  const toggleWatcher = watch("mode");
  const signUpValues = watch("signUp");
  const signInValues = watch("signIn");

  const handleRegistrationClick = () => {
    signUp({
      username: signUpValues.username || "",
      password: signUpValues.password || "",
    });
  };

  const handleLoginClick = () => {
    signIn({
      username: signInValues.username || "",
      password: signInValues.password || "",
    });
  };
  return {
    handleRegistrationClick,
    handleLoginClick,
    isLogError,
    isRegError,
    toggleWatcher,
    control,
  };
};
