// TODO: (AM) getUrl
export const ROUTES = {
  AUTH: "/authentication",
  HOME: "/home",
  FEATURES: {
    TESTS: {
      INDEX: "/tests",
      CREATE: "create",
      EDIT: "edit",
      TAKE: "take",
    },
    FEED: "/feed",
  },
  USERS: "users",
  ABOUT: {
    INDEX: "/about",
    CV: "cv",
    PORTFOLIO: "portfolio",
    SKILLS: "skills",
  },
} as const;
