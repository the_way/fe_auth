import { baseApi } from "./base";

export const authApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    signUp: build.mutation({
      query: (body) => ({
        url: "auth/registration",
        method: "POST",
        body,
      }),
    }),
    signIn: build.mutation({
      query: (body) => ({
        url: "auth/login",
        method: "POST",
        body,
      }),
    }),
    check: build.query({
      query: () => ({
        url: "auth/check",
        method: "GET",
      }),
    }),
  }),
});
