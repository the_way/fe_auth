import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQueryWithToken } from "./base-query-with-token";

export const baseApi = createApi({
  reducerPath: "api",
  baseQuery: baseQueryWithToken,
  endpoints: () => ({}),
});
