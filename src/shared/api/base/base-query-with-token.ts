import { fetchBaseQuery } from "@reduxjs/toolkit/query";
import { ENV } from "shared/config/env";

export const baseQueryWithToken = fetchBaseQuery({
  baseUrl: `${ENV.BASE_API}/api/`,

  prepareHeaders: (headers) => {
    const token = localStorage.getItem("dop_token");
    if (token) {
      headers.append("Authorization", `Bearer ${token}`);
    }
    headers.append("content-type", "application/json");
    headers.append("Accept", "application/json");
    return headers;
  },
});
