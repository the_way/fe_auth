import { selectAuthorized, setAuthorized } from "../../features/auth";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet, useLocation, useNavigate } from "react-router-dom";

import { ROUTES } from "../../shared/config/routes";
import { authApi } from "../../shared/api";
import { PUBLIC_ROUTES } from "./public-routes";

export const AuthOutlet = () => {
  const dispatch = useDispatch();
  const navTo = useNavigate();
  const location = useLocation();
  const { data, isSuccess, isLoading, isError } =
    authApi.useCheckQuery("/auth/check");
  const isAuthorized = useSelector(selectAuthorized);
  if (isSuccess && data?.isAuth && !isAuthorized) {
    dispatch(setAuthorized(true));
  }

  useEffect(() => {
    if (
      (isSuccess || isError) &&
      !isAuthorized &&
      !data?.isAuth &&
      !PUBLIC_ROUTES.includes(location.pathname)
    ) {
      navTo(ROUTES.AUTH);
    }
  }, [location.pathname, isAuthorized, isLoading]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return <Outlet />;
};
