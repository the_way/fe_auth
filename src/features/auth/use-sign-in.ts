// import type { TAuthPageProps } from "pages/auth/auth-page";
import { useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import type { AnyAction } from "redux";
import { authApi } from "../../shared/api";
import type { GlobalStore } from "../../app/store/root-reducer";

export type TAuthPageProps = {
  setAuthorized?: (flag: boolean) => AnyAction;
  ROUTES?: { HOME: string };
  selectAuthorized?: (state: GlobalStore) => boolean;
};
type TLoginRequest = { username: string; password: string };

// TODO: (AM) не дублировать типы
export const useSignIn = (props: TAuthPageProps) => {
  const navTo = useNavigate();
  const dispatch = useDispatch();
  const [signInMutation, { data, isSuccess, isError, error }] =
    authApi.useSignInMutation();
  const signIn = useCallback(
    async (loginRequest: TLoginRequest) => {
      await signInMutation(loginRequest);
    },
    [signInMutation]
  );

  useEffect(() => {
    if (isSuccess) {
      if (props && props?.setAuthorized) dispatch(props?.setAuthorized(true));
      localStorage.setItem("dop_token", data.token);
      if (props && props?.ROUTES) {
        navTo(props.ROUTES.HOME);
      }
    }
  }, [dispatch, isSuccess]);

  return {
    signIn,
    isSuccess,
    isError,
    errorText: isError ? error : undefined,
  };
};
