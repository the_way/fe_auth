import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import type { GlobalStore } from './../../../app/store/root-reducer'

type TState = {
  isAuthorized: boolean;
};

const initialState = {
  isAuthorized: false,
} as TState;

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setAuthorized(state, action: PayloadAction<boolean>) {
      // eslint-disable-next-line no-param-reassign
      state.isAuthorized = action.payload;
    },
  },
});

export const { setAuthorized } = authSlice.actions;
export const { reducer: authReducer } = authSlice;

export const selectAuthorized = (state: GlobalStore) => state.auth.isAuthorized;
