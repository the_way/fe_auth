import { Paper, styled } from "@mui/material";
import type { ReactNode } from "react";

type TAuthBoxProps = {
  children: ReactNode;
};

export const StyledPaper = styled(Paper)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  padding: theme.spacing(2),
  rowGap: "10px",
}));

export const StyledForm = styled("form")(() => ({
  display: "grid",
  justifyContent: "center",
  "& > :not(style)": {
    width: 340,
    height: 360,
  },
}));

export const AuthBox = ({ children }: TAuthBoxProps) => {
  return (
    <StyledForm>
      <StyledPaper elevation={3}>{children}</StyledPaper>
    </StyledForm>
  );
};
