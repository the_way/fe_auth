// TODO: (AM) убрать лишние и public api
export { store } from "./src/app/store/store";

export {
  authReducer,
  selectAuthorized,
  setAuthorized,
} from "./src/features/auth/model/auth-slice";
export { authApi } from "./src/shared/api/auth-api";
export { baseApi } from "./src/shared/api/base/base-api";
export { AuthPage } from "./src/pages/auth/auth-page";
export { AuthOutlet } from "./src/app/router/auth-outlet";
